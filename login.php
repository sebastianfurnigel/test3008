<?php
error_reporting(E_ALL & ~E_NOTICE);
session_start();
if ($_POST['submit']) {
    require_once("connection.php");
    $username = $_POST['email'];
    $password = $_POST['password'];
    $sql = "SELECT id, email, password FROM users where email='$username' AND admin = '1' LIMIT 1";
    $query = mysqli_query($connect, $sql);

    if ($query) {
        $row = mysqli_fetch_row($query);
        $userId = $row[0];
        $db_username = $row[1];
        $db_password = $row[2];
    }

    if ($username == $db_username && $password == $db_password) {
        $_SESSION['email'] = $username;
        $_SESSION['id'] = $userId;
        header('Location: index.php');
    } else {
        echo "Invalid.";
    }
}


?>

<!DOCTYPE html>
<html>
<head>
  <title>Login</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="style.css" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<form method="post" action="login.php">
<table class="table-sm">
  <tr>
    <th class="lead"> Email:</th>
    <th class="lead"> Password: </th>
  </tr>
  <tr>
    <td><input type="email" name="email"></td>
    <td><input type="password" name="password"></td>
    <td><input type="submit" class="btn btn-success" name="submit" value="Login">
  </tr>
</table>
<br><br>



</body>
</html>