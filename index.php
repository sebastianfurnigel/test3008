<?php
error_reporting(E_ALL & ~E_NOTICE);
session_start();
require_once"connection.php";
$answers = "SELECT * FROM answers";
$sql_answers = $connect->query($answers);

if (isset($_SESSION['id'])) {
    $userId = $_SESSION['id'];
    $username = $_SESSION['email'];
} else {
    header('Location: login.php');
    die();
}


if (isset($_POST['submit'])) {
    $a_1 = $_POST['A_1'];
    $a_2 = $_POST['A_2'];
    $a_3_1 = $_POST['A_3_1'];
    $a_3_2 = $_POST['A_3_2'];
    $a_4 = $_POST['A_4'];
    $b_1_1 = $_POST['B_1_1'];
    $b_1_2 = $_POST['B_1_2'];
    $b_2_1 = $_POST['B_2_1'];
    $b_2_2 = $_POST['B_2_2'];
    $b_2_3 = $_POST['B_2_3'];
    $b_2_4 = $_POST['B_2_4'];
    $b_3_1 = $_POST['B_3_1'];
    $b_3_2 = $_POST['B_3_2'];
    $b_3_3 = $_POST['B_3_3'];
    $b_3_4 = $_POST['B_3_4'];
    $b_3_5 = $_POST['B_3_5'];
    $b_3_6 = $_POST['B_3_6'];
    $b_3_7 = $_POST['B_3_7'];
    $c_1_1 = $_POST['C_1_1'];
    $c_1_2 = $_POST['C_1_2'];
    $c_2 = $_POST['C_2'];




    $input_answer = "INSERT INTO answers (userId, A_1, A_2, A_3_1, A_3_2, A_4, B_1_1, B_1_2, B_2_1, B_2_2, B_2_3, B_2_4, B_3_1, B_3_2, B_3_3, B_3_4, B_3_5, B_3_6, B_3_7, C_1_1, C_1_2, C_2) VALUES ('$userId', '$a_1', '$a_2', '$a_3_1', '$a_3_2', '$a_4', '$b_1_1', '$b_1_2', '$b_2_1', '$b_2_2', '$b_2_3', '$b_2_4', '$b_3_1', '$b_3_2', '$b_3_3', '$b_3_4', '$b_3_5', '$b_3_6', '$b_3_7', '$c_1_1', '$c_1_2', '$c_2')";
    echo $input_answer;
    $sql_input_answer = $connect->query($input_answer);
    if ($sql_input_answer) {
            header( 'Location: index.php' );
            exit();
        }

}
var_dump($userId);

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Chestionar</title>
    <link rel="stylesheet" href="css/style.css" type="text/css">
</head>

<body>

<form action="logout.php">
    <input type="submit" value="Log Out." name="logout" />
</form>
<form action="" method="post">
<div class="sectiune">
    <p><b>SECŢIUNEA A – Evaluare potențial </b></p>
    <p><strong>IMPORTANT! DATELE COMPLETATE VOR FI NOTATE ÎN CIFRE, <u>NU PROCENTE!</u></strong></p>
    <div class="container-sectiune">
        <ol>
            <li>
                Care este numarul total de pacienti pe care ii consultati lunar, in medie? <br><input type="number" name="A_1"></input> pacienţi
            </li>
            <li>
                Câţi pacienţi diagnosticaţi cu <strong>dislipidemie</strong> se adreseaza la dvs. la cabinet lunar in medie, pentru control sau tratament? (număr total)<br> <input type="number" id="2" name="A_2"></input> pacienţi
            </li>
            <li>
                Dintre aceștia câți sunt <strong>pacienți noi</strong> și câți sunt pe <strong>terapie de continuare?</strong><br> <input type="number" name="A_3_1"></input> pacienţi noi <br> <input type="number" name="A_3_2"></input> pe terapie de continuare
            </li>
            <li>
                Din numărul total de pacienţi cu <strong>dislipidemie</strong> (2) câți sunt tratați tratați cu <strong>statine</strong>?
                <br> <input type="number" name="A_4"></input> pacienţi
            </li>

        </ol>
    </div>
</div>
<br>

<div class="sectiune">
    <p><strong>SECŢIUNEA B – Relația medic-pacient </strong></p>
    <div class="container-sectiune">
        <table class="second-table">
            <tr>
                <th><strong>1. Cum apreciați că este timpul alocat de dvs. comunicării cu pacientul, în general, în ultimul an:… <br>RĂSPUNS UNIC PE LINIE!</strong></th>
                <th>Suficient</th>
                <th>Insuficient</th>
                <th>NU ŞTIE</th>
            </tr>
            <tr>
                <th>1. Unui pacient nou, la prima vizită</th>
                <th><input type="radio" name="B_1_1" value="1">1</th>
                <th><input type="radio" name="B_1_1" value="2">2</th>
                <th><input type="radio" name="B_1_1" value="88">88</th>
            </tr>
            <tr>
                <th>2. Unui pacient la control</th>
                <th><input type="radio" name="B_1_2" value="1">1</th>
                <th><input type="radio" name="B_1_2" value="2">2</th>
                <th><input type="radio" name="B_1_2" value="88">88</th>
            </tr>
        </table>

        <table class="second-table">
            <tr>
                <th>2. Cât timp alocați într-o consultație obișnuită pentru discutarea următoarelor aspecte:… RĂSPUNS UNIC PE LINIE!</th>
                <th>Procent</th>
            </tr>
            <tr>
                <th>1. Discutarea aspectelor bio-medicale (informații legate de boală și tratament așa cum reies ele din experiența medicului, ghiduri de tratament etc.)</th>
                <th><input type="number" name="B_2_1" min="0" max="100">%</th>
            </tr>
            <tr>
                <th>2. Discutarea aspectelor psiho-sociale (informații despre statusul social al pacientului, starea emoțională, îngrijorările acestora etc.)</th>
                <th><input type="number" name="B_2_2" min="0" max="100">%</th>
            </tr>
            <tr>
                <th>3. Discutarea aspectelor administrative legate de tratament </th>
                <th><input type="number" name="B_2_3" min="0" max="100">%</th>
            </tr>
            <tr>
                <th>88. Nu știe</th>
                <th><input type="number" name="B_2_4" min="0" max="100">%</th>
            </tr>
            <tr>
                <th></th>
                <th>100%</th>
            </tr>
        </table>

        <table class="second-table">
            <tr>
                <th>3. În ce măsură considerați că următoarele sunt bariere în construirea unei bune relații și comunicări cu pacientul: RĂSPUNS UNIC PE LINIE!</th>
                <th>Foarte mare măsură</th>
                <th>Mare măsură</th>
                <th>Mică măsură</th>
                <th>Foarte mică măsură/ deloc</th>
                <th>NU ŞTIE</th>
            </tr>
            <tr>
                <th>1. Timpul insuficient petrecut de medic cu pacientul în timpul consultației</th>
                <th>1<input type="radio" name="B_3_1" value="1"></th>
                <th>2<input type="radio" name="B_3_1" value="2"></th>
                <th>3<input type="radio" name="B_3_1" value="3"></th>
                <th>4<input type="radio" name="B_3_1" value="4"></th>
                <th>88<input type="radio" name="B_3_1" value="88"></th>
            </tr>
            <tr>
                <th>2. Pacientul nu înțelege planul de tratament agreat</th>
                <th>1<input type="radio" name="B_3_2" value="1"></th>
                <th>2<input type="radio" name="B_3_2" value="2"></th>
                <th>3<input type="radio" name="B_3_2" value="3"></th>
                <th>4<input type="radio" name="B_3_2" value="4"></th>
                <th>88<input type="radio" name="B_3_2" value="88"></th>
            </tr>
            <tr>
                <th>3. Pacientul nu înțelege gravitatea bolii</th>
                <th>1<input type="radio" name="B_3_3" value="1"></th>
                <th>2<input type="radio" name="B_3_3" value="2"></th>
                <th>3<input type="radio" name="B_3_3" value="3"></th>
                <th>4<input type="radio" name="B_3_3" value="4"></th>
                <th>88<input type="radio" name="B_3_3" value="88"></th>
            </tr>
            <tr>
                <th>4. Dificultatea tratamentului (administrare etc.)</th>
                <th>1<input type="radio" name="B_3_4" value="1"></th>
                <th>2<input type="radio" name="B_3_4" value="2"></th>
                <th>3<input type="radio" name="B_3_4" value="3"></th>
                <th>4<input type="radio" name="B_3_4" value="4"></th>
                <th>88<input type="radio" name="B_3_4" value="88"></th>
            </tr>
            <tr>
                <th>5. Lipsa unei comunicări eficiente medic-pacient</th>
                <th>1<input type="radio" name="B_3_5" value="1"></th>
                <th>2<input type="radio" name="B_3_5" value="2"></th>
                <th>3<input type="radio" name="B_3_5" value="3"></th>
                <th>4<input type="radio" name="B_3_5" value="4"></th>
                <th>88<input type="radio" name="B_3_5" value="88"></th>
            </tr>
            <tr>
                <th>6. Pregătirea insuficientă a medicilor în ceea ce privește comunicarea cu pacientul</th>
                <th>1<input type="radio" name="B_3_6" value="1"></th>
                <th>2<input type="radio" name="B_3_6" value="2"></th>
                <th>3<input type="radio" name="B_3_6" value="3"></th>
                <th>4<input type="radio" name="B_3_6" value="4"></th>
                <th>88<input type="radio" name="B_3_6" value="88"></th>
            </tr>
            <tr>
                <th>7. Aspectele administrativ-birocratice presupuse de tratament</th>
                <th>1<input type="radio" name="B_3_7" value="1"></th>
                <th>2<input type="radio" name="B_3_7" value="2"></th>
                <th>3<input type="radio" name="B_3_7" value="3"></th>
                <th>4<input type="radio" name="B_3_7" value="4"></th>
                <th>88<input type="radio" name="B_3_7" value="88"></th>
            </tr>
        </table>

    </div>
</div>

<div class="sectiune">
    <p><strong>SECŢIUNEA C</strong></p>
    <div class="container-section">
        <div>
            <strong>1. Cât de utile considerați că sunt pe dvs… (câte un răspuns pe fiecare linie)?</strong>
        </div>

        <table class="second-table">
            <tr>
                <th></th>
                <th>Foarte utile</th>
                <th>Utile</th>
                <th>Putin utile</th>
                <th>Foarte puțin utile/ deloc</th>
                <th>Nu știu</th>
            </tr>
            <tr>
                <th>1. Modalitățile clasice de promovare (vizitele față în față ale reprezentanților medicali, simpozioane, materiale printate etc.)</th>
                <th>1<input type="radio" name="C_1_1" value="1"></th>
                <th>2<input type="radio" name="C_1_1" value="2"></th>
                <th>3<input type="radio" name="C_1_1" value="3"></th>
                <th>4<input type="radio" name="C_1_1" value="4"></th>
                <th>88<input type="radio" name="C_1_1" value="88"></th>
            </tr>
            <tr>
                <th>2. Modalitățile inovative de promovare (comunicare prin email, evenimente online, webcast-uri, platforme digitale, prezentări interactive pe tablete etc.)</th>
                <th>1<input type="radio" name="C_1_2" value="1"></th>
                <th>2<input type="radio" name="C_1_2" value="2"></th>
                <th>3<input type="radio" name="C_1_2" value="3"></th>
                <th>4<input type="radio" name="C_1_2" value="4"></th>
                <th>88<input type="radio" name="C_1_2" value="88"></th>
            </tr>
        </table>
    </div>

    <ol>
        <li class="radioicons">
            <strong>Dacă este să vă gândiți la comportamentul dvs. de până acum vizavi de acest tip de produse, cum considerați <span>că ați procedat</span> în legătură cu aceste produse inovative? Vă rog să folosiți o scală de la 1 la 5 unde 1 înseamnă Foarte rapid de la lansare iar 5 Foarte târziu de la lansare. </strong>
            <br/><br>
            <input type="radio" id="C_2_1" name="C_2" value="1">
                <label for="C_2_1">1. Foarte rapid de la lansare</label><br>
            <input type="radio" id="C_2_2" name="C_2" value="2">
                <label for="C_2_2">2. Rapid de la lansare </label><br>
            <input type="radio" id="C_2_3" name="C_2" value="3">
                <label for="C_2_3">3. Nici rapid nici târziu de la lansare </label><br>
            <input type="radio" id="C_2_4" name="C_2" value="4">
                <label for="C_2_4">4. Târziu de la lansare </label><br>
            <input type="radio" id="C_2_5" name="C_2" value="5">
                <label for="C_2_5">5. Foarte târziu de la lansare</label><br>
            <input type="radio" id="C_2_6" name="C_2" value="6">
                <label for="C_2_6">6. Nu am avut astfel de situații în trecut</label><br>
            <input type="radio" id="C_2_88" name="C_2" value="88">
                <label for="C_2_88">88. Nu știu </label><br>

        </li>
    </ol>
</div>


<div id="button"><button type="submit" value="submit" name="submit">Submit</button></div>
</form>
</body>
</html>